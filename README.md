# app challenge para la empresa TRINOMIO

## En esta aplicación podemos observar como la api de mercado libre nos provee las categorías del sito MercadoLibre Argentina.

## En esta app se usó VueJS, con las librerías Vuetify, para la visualización, y axios para poder interactuar con la api. Si bien las mismas dependencias necesarias las podemos encontrar en el archivo Package.json:

- axios: ^0.18.0
- vue: ^2.5.21
- vue-router: ^3.0.1
- vuetify: ^1.4.1


## Pasos para poder correr la aplicación


1) clonar el repositorio


2) Una vez localizados dentro del directorio por consola, ejecutar los siguientes comandos


3) npm install (Para poder descargar todas las librerías necesarias por la aplicación en VueJS)


4) npm run serve (Para poder levantar el servidor)


5) Abrir en un browser el http://LocalHost:8080/


